@file:Suppress("DEPRECATION")

package endetect.com.endetect.activity


import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.content.Intent
import android.app.Activity
import android.R.string.cancel
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.support.v7.widget.Toolbar
import android.view.inputmethod.InputMethodManager
import android.app.ProgressDialog
import android.net.NetworkInfo
import android.net.ConnectivityManager
import android.view.Gravity
import android.R.attr.gravity
import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.net.wifi.WifiManager
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.widget.*
import java.net.NoRouteToHostException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.text.SimpleDateFormat
import java.util.*


open class BaseActivity : AppCompatActivity() , View.OnClickListener {


    protected var toolbar: Toolbar? = null
    var progressDialog: ProgressDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        progressDialog = ProgressDialog(this)
        //   setContentView(R.layout.activity_base)
    }

    fun initToolBar(title: String) {
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.setTitle(title)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        //   supportActionBar!!.setHomeAsUpIndicator(getHomeIcon())
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        //   toolbar!!.setTitleTextColor(resources.getColor(R.color.white))
    }

    fun showProgress() {
        progressDialog!!.setMessage("Please wait...")
        progressDialog!!.setIndeterminate(true)
        progressDialog!!.setCancelable(false)
        progressDialog!!.show()
    }

    fun dissmissProgress() {
       // progressDialog = ProgressDialog(this)
        progressDialog!!.dismiss()
    }

    override fun onClick(p0: View?) {
    }

    fun showToast(message: String) {
        if (!TextUtils.isEmpty(message))
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun setText(text: String, textViewId: Int) {
        val view = findViewById<View>(textViewId) as TextView
        view.text = text
    }

    fun setText(text: String, textViewId: Int, v: View) {
        val view = v.findViewById(textViewId) as TextView
        view.text = text
    }

    fun startActivityWithClearStack(activityClass: Class<out Activity>) {
        val i = Intent(this, activityClass)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(i)
    }

    fun throwExceptions(e: Exception, coordinatorLayout: CoordinatorLayout) {
        if (e is UnknownHostException) {
            setSnackbarCoordinate("Invalid url...please try again", coordinatorLayout)
        } else if (e is NoRouteToHostException) {
            setSnackbarCoordinate("URL not found...please try again", coordinatorLayout)
        } else if (e is SocketTimeoutException) {
            setSnackbarCoordinate("Time out...please try again", coordinatorLayout)
        }else if (e is NullPointerException){
            setSnackbarCoordinate("Null value exception...please try again", coordinatorLayout)
        } else {
            setSnackbarCoordinate("some error...please try again", coordinatorLayout)
        }
    }

    fun startNextActivity(activityClass: Class<out Activity>) {
        val i = Intent(this, activityClass)
        startActivity(i)
    }

    fun startNextActivityForResult(activityClass: Class<out Activity>, reqCode: Int) {
        val i = Intent(this, activityClass)
        startActivityForResult(i, reqCode)
    }

    protected fun setOnClickListener(vararg viewIds: Int) {
        for (i in viewIds) {
            findViewById<View>(i).setOnClickListener(this)
        }
    }

    protected fun getEditText(editTextId: Int): String {
        return (findViewById<View>(editTextId) as EditText).text.toString()
    }

    fun showAlertDialog(context: Context, message: String) {
        val builder = AlertDialog.Builder(context)
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })
        val alert = builder.create()
        alert.show()
    }

    @SuppressLint("MissingPermission")
    fun isInternetConnected(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    fun hideSoftKeyboard() {
        if (currentFocus != null) {
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
    }

    fun setSnackbarCoordinate(message: String, coordinatorLayout: CoordinatorLayout) {
        val snackbar = Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT)
        val view = snackbar.view
        val params = view.layoutParams as CoordinatorLayout.LayoutParams
       // params.gravity = Gravity.TOP
        view.layoutParams = params
        params.gravity = Gravity.TOP or Gravity.CENTER_HORIZONTAL
        params.width = FrameLayout.LayoutParams.MATCH_PARENT
        snackbar.show()
    }

    @SuppressLint("WifiManagerLeak")
    fun isWiFiEnabled(boolean: Boolean){
        val wifi = getSystemService(Context.WIFI_SERVICE) as WifiManager
        wifi.isWifiEnabled = boolean
    }



}

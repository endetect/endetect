package endetect.com.endetect.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import endetect.com.endetect.R;
import endetect.com.endetect.Volley.VolleyAllRequest;
import endetect.com.endetect.Volley.VolleyCallback;
import endetect.com.endetect.utity.AllAsyncTaskRequest;
import endetect.com.endetect.utity.ApiCallBack;
import endetect.com.endetect.utity.AppConstant;
import endetect.com.endetect.utity.MyClickListner;
import endetect.com.endetect.utity.ServiceApi;
import endetect.com.endetect.utity.SessionManager;
import in.ddinfotech.shiksha.Util.PrefencesManager;

public class LoginActivity extends BaseActivity {
    Typeface tvFontIcon;
    private static final String TAG = LoginActivity.class.getSimpleName();
    CoordinatorLayout activity_candidate_login;
    EditText eusername, epassword;
    Button blogin;
    ImageView userPicture;
    PrefencesManager prefencesManager;
    TextView passwordIcon, userIcon;
    String rollNumber, dateOfBirth;
    ApiCallBack apiCallBack;
    private TextInputLayout inputLayoutEmail, inputLayoutPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
        initResponse();

        String android_id = Settings.Secure.getString(LoginActivity.this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        prefencesManager.saveString(AppConstant.PreferenceKey.DEVICE_ID, android_id);

    }

    private void initView() {
        prefencesManager = new PrefencesManager(LoginActivity.this);
        tvFontIcon = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
        activity_candidate_login = findViewById(R.id.activity_candidate_login);
        epassword = findViewById(R.id.etPassword);
        inputLayoutEmail = (TextInputLayout) findViewById(R.id.til_asignin_Email);
        inputLayoutPassword = (TextInputLayout) findViewById(R.id.til_asignin_pass);
        eusername = findViewById(R.id.etEmail);
        setOnClickListener(R.id.login_button);
        epassword.setOnClickListener(this);
        eusername.addTextChangedListener(new MyTextWatcher(eusername));
        epassword.addTextChangedListener(new MyTextWatcher(epassword));
    }

    @Override
    public void onClick(@Nullable View p0) {
        super.onClick(p0);
        int id = p0.getId();
        switch (id) {
            case R.id.login_button: {
                String username = getEditText(R.id.etEmail);
                String password = getEditText(R.id.etPassword);
                if (checkValidationSignIn()) {
                    showProgress();
                    new AllAsyncTaskRequest(LoginActivity.this, apiCallBack).setListener(AppConstant.ApiCall.CANDIDATE_LOGIN, 1).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, username, password, prefencesManager.getString(AppConstant.PreferenceKey.DEVICE_ID));
                }
                break;
            }


        }
    }

//    public void newLogin(String username, String password) {
//        if (isInternetConnected()) {
//            loginData loginData1 = new loginData();
//            loginData1.execute(username, password);
//        } else {
//            Toast.makeText(LoginActivity.this, "You have not connect with your internet, check your internet connection then try again.", Toast.LENGTH_SHORT).show();
//        }
//    }

//    public class loginData extends AsyncTask<String, String, String> {
//        ServiceApi serviceApi = new ServiceApi(getApplicationContext());
//        String result;
//
//        @Override
//        public void onPreExecute() {
//            super.onPreExecute();
//            showProgress();
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//            try {
//                result = serviceApi.loginUser(strings[0], strings[1]);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//
//            dissmissProgress();
//            if (result != null) {
//                try {
//
//                    JSONArray jsonArray = new JSONArray(result);
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        JSONObject jsonObject = jsonArray.getJSONObject(i);
//                        String status = jsonObject.getString("status");
//                        if (status.equalsIgnoreCase("1")) {
//                            String ownerid = jsonObject.getString("ownerid");
//                            String name = jsonObject.getString("name");
//                            String emailid = jsonObject.getString("emailid");
//                            String mobile = jsonObject.getString("mobile");
//                            prefencesManager.saveString(AppConstant.PreferenceKey.USER_NAME, name);
//                            prefencesManager.saveString(AppConstant.PreferenceKey.QNWER_ID, ownerid);
//                            prefencesManager.saveString(AppConstant.PreferenceKey.EMAIL, emailid);
//                            prefencesManager.saveString(AppConstant.PreferenceKey.MOBILE, mobile);
//                        } else {
//                            Toast.makeText(LoginActivity.this, "Pleaase check User Name and Passwod", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//
//                    startActivity(new Intent(LoginActivity.this, DashBoardActivity.class));
//                    SessionManager sessionManager = new SessionManager(LoginActivity.this);
//                    sessionManager.createLoginSession(prefencesManager.getString(AppConstant.PreferenceKey.USER_NAME), prefencesManager.getString(AppConstant.PreferenceKey.EMAIL));
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                    Toast.makeText(getApplicationContext(), "some error...", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//        }
//    }

    private void initResponse() {
        this.apiCallBack = new ApiCallBack() {
            @Override
            public void callBackResponse(String result, int requestCode) {
                switch (requestCode) {
                    case 1:
                        dissmissProgress();
                        System.out.println("result" + result);

                        if (result != null && !result.equals("")) {
                            try {

                                JSONArray jsonArray = new JSONArray(result);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    String status = jsonObject.getString("status");
                                    if (status.equalsIgnoreCase("1")) {
                                        String ownerid = jsonObject.getString("ownerid");
                                        String name = jsonObject.getString("name");
                                        String emailid = jsonObject.getString("emailid");
                                        String mobile = jsonObject.getString("mobile");
                                        prefencesManager.saveString(AppConstant.PreferenceKey.USER_NAME, name);
                                        prefencesManager.saveString(AppConstant.PreferenceKey.QNWER_ID, ownerid);
                                        prefencesManager.saveString(AppConstant.PreferenceKey.EMAIL, emailid);
                                        prefencesManager.saveString(AppConstant.PreferenceKey.MOBILE, mobile);
                                        prefencesManager.saveString(AppConstant.PreferenceKey.pageNum, "0");
                                        prefencesManager.saveString(AppConstant.PreferenceKey.FEEDNO, "1533036072");
                                        prefencesManager.saveString(AppConstant.PreferenceKey.ENDUSERID, "");
                                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                        SessionManager sessionManager = new SessionManager(LoginActivity.this);
                                        sessionManager.createLoginSession(prefencesManager.getString(AppConstant.PreferenceKey.USER_NAME), prefencesManager.getString(AppConstant.PreferenceKey.EMAIL));

                                    } else {
                                        Toast.makeText(LoginActivity.this, "Pleaase check User Name and Passwod", Toast.LENGTH_SHORT).show();
                                    }
                                }


                            } catch (Exception ex) {
                                ex.printStackTrace();
                                Toast.makeText(getApplicationContext(), "some error...", Toast.LENGTH_SHORT).show();
                            }
                        }
                        break;

                }
            }

            @Override
            public void callBackError(final Exception exception, final int requestCode) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dissmissProgress();
                        throwExceptions(exception, activity_candidate_login);
                        System.out.println("" + exception);
                    }
                });
            }
        };
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.etEmail:
                    validateEmail();
                    break;
                case R.id.etPassword:
                    validatePassword();
                    break;
            }
        }
    }

    private boolean validateEmail() {
        String email = eusername.getText().toString().trim();
        if (email.isEmpty() || !isValidEmail(email)) {
            inputLayoutEmail.setError(getString(R.string.err_msg_email));
            requestFocus(eusername);
            return false;
        } else {
            inputLayoutEmail.setErrorEnabled(false);
        }
        return true;
    }

    private static boolean isValidEmail(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean validatePassword() {
        if (epassword.getText().toString().trim().isEmpty()) {
            inputLayoutPassword.setError(getString(R.string.err_msg_password));
            requestFocus(epassword);
            return false;
        } else {
            inputLayoutPassword.setError(null);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private boolean checkValidationSignIn() {

        if (!validateEmail()) {
            Log.d("msg", "not valid email");
            return false;
        }

        if (!validatePassword()) {
            Log.d("msg", "not valid pass");
            return false;
        }
        return true;
    }
}



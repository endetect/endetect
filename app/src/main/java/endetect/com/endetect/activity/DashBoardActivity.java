//package endetect.com.endetect.activity;
//
//import android.app.AlertDialog;
//import android.content.Intent;
//import android.graphics.Color;
//import android.graphics.Typeface;
//import android.graphics.drawable.ColorDrawable;
//import android.graphics.drawable.Drawable;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.design.widget.CoordinatorLayout;
//import android.support.v4.widget.DrawerLayout;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.GridLayoutManager;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.text.Html;
//import android.util.Log;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import org.jetbrains.annotations.Nullable;
//import org.json.JSONArray;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.Calendar;
//
//import endetect.com.endetect.R;
//import endetect.com.endetect.RecylerObject.DashBoardObject;
//import endetect.com.endetect.RecylerObject.SectionListObject;
//import endetect.com.endetect.adapter.EndUserAdapter;
//import endetect.com.endetect.adapter.DashBoardAdapter;
//import endetect.com.endetect.adapter.SectionListAdapter;
//import endetect.com.endetect.utity.AllAsyncTaskRequest;
//import endetect.com.endetect.utity.ApiCallBack;
//import endetect.com.endetect.utity.AppConstant;
//import endetect.com.endetect.utity.MyClickListner;
//import endetect.com.endetect.utity.SessionManager;
//import in.ddinfotech.shiksha.Util.PrefencesManager;
//
//public class DashBoardActivity extends BaseActivity {
//    DrawerLayout drawer_layout;
//    RelativeLayout rlDrawer;
//    SectionListAdapter adapter;
//    // MyClickListner myClickListner;
//    boolean isSectionPosition = true;
//    int sectionPosition = 0, newSectionPosition = 0;
//    DashBoardAdapter dashBoardAdapter;
//    EndUserAdapter adapter2;
//    Typeface typeface;
//    ImageView barTextView, barTextViewRightSide;
//    ArrayList<SectionListObject> sectionList = new ArrayList<>();
//    ArrayList<DashBoardObject> mainList = new ArrayList<>();
//    //ArrayList<QuestionSetObject> newQuestionList = new ArrayList<>();
//    RecyclerView recyclerViewDrawer, recycler_main, recyclerViewDrawerRight;
//    ApiCallBack apiCallBack;
//    PrefencesManager prefencesManager;
//    CoordinatorLayout activity_candidate_Dashboard;
//    SessionManager sessionManager;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        typeface = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
//        setContentView(R.layout.activity_main);
//        initView();
//
//        initRecyclerViewRightSide();
//        initRecyclerView();
//        initResponse();
//        initRecyclerViewMain();
//    }
//
//    public void LoadData() {
//        new AllAsyncTaskRequest(DashBoardActivity.this, apiCallBack).setListener(AppConstant.ApiCall.GET_FEEDS, 1).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, prefencesManager.getString(AppConstant.PreferenceKey.QNWER_ID), "", "");
//        // new AllAsyncTaskRequest(DashBoardActivity.this, apiCallBack).setListener(AppConstant.ApiCall., 1).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, prefencesManager.getString(AppConstant.PreferenceKey.QNWER_ID));
//        //new AllAsyncTaskRequest(DashBoardActivity.this, apiCallBack).setListener(AppConstant.ApiCall.GET_FEEDS, 1).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, prefencesManager.getString(AppConstant.PreferenceKey.QNWER_ID));
//
//    }
//
//    public void initView() {
//        sessionManager = new SessionManager(DashBoardActivity.this);
//        prefencesManager = new PrefencesManager(DashBoardActivity.this);
//        drawer_layout = findViewById(R.id.drawer_layout);
//        activity_candidate_Dashboard = findViewById(R.id.activity_candidate_Dashboard);
//        rlDrawer = findViewById(R.id.rlDrawer);
//        barTextView = findViewById(R.id.barTextView);
//        barTextViewRightSide = findViewById(R.id.barTextViewRightSide);
//        recyclerViewDrawer = findViewById(R.id.recyclerViewDrawer);
//        recycler_main = findViewById(R.id.rv_main);
//        recyclerViewDrawerRight = findViewById(R.id.recyclerViewDrawerRight);
//        setOnClickListener(R.id.barTextView, R.id.barTextViewRightSide);
//
////        drawer_layout.addDrawerListener(new DrawerLayout.DrawerListener() {
////            @Override
////            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
////            }
////
////            @Override
////            public void onDrawerOpened(@NonNull View drawerView) {
////                //drawerLayoutButton();
////            }
////
////            @Override
////            public void onDrawerClosed(@NonNull View drawerView) {
////            }
////
////            @Override
////            public void onDrawerStateChanged(int newState) {
////            }
////        });
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//
//        (adapter).setOnItemClickListner(new MyClickListner() {
//            @Override
//            public void onItemClick(int position, View view) {
//                selectItem(position, view);
//                if (position == 4) {
//                    sessionManager.logoutUser();
//                }else if ((position==3))
//                {
//                    startNextActivity(ChangePasswordActivity.class);
//                    finish();
//                }else if ((position==1))
//                {
//                   Toast.makeText(DashBoardActivity.this,"Some error with server",Toast.LENGTH_SHORT).show();
//                }
//                drawer_layout.closeDrawers();
//            }
//        });
//        (adapter2).setOnItemClickListner(new MyClickListner() {
//            @Override
//            public void onItemClick(int position, View view) {
//                // selectQuestion(position, view);
//                drawer_layout.closeDrawers();
//            }
//        });
//    }
//
//    private void initRecyclerView() {
//        SectionListObject o = new SectionListObject(getResources().getDrawable(R.drawable.dashboard_activity), "DashBoard/Activity");
//        SectionListObject o1 = new SectionListObject(getResources().getDrawable(R.drawable.ref), "Live");
//        SectionListObject o2 = new SectionListObject(getResources().getDrawable(R.drawable.manage_user), "Maager User");
//        SectionListObject o3 = new SectionListObject(getResources().getDrawable(R.drawable.change_password), "Change Password");
//        SectionListObject o4 = new SectionListObject(getResources().getDrawable(R.drawable.candidate_pic), "Logout");
//        sectionList.add(o);
//        sectionList.add(o1);
//        sectionList.add(o2);
//        sectionList.add(o3);
//        sectionList.add(o4);
//        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
//        recyclerViewDrawer.setLayoutManager(manager);
//        recyclerViewDrawer.setHasFixedSize(true);
//        adapter = new SectionListAdapter(this, sectionList);
//        recyclerViewDrawer.setAdapter(adapter);
//    }
//
//    private void initRecyclerViewRightSide() {
//        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
//        recyclerViewDrawerRight.setLayoutManager(manager);
//        recyclerViewDrawerRight.setHasFixedSize(true);
////        adapter2 = new EndUserAdapter(this, 2);
//        recyclerViewDrawerRight.setAdapter(adapter2);
//    }
//
//    private void initRecyclerViewMain() {
//        LoadData();
//        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
//        recycler_main.setLayoutManager(manager);
//        recycler_main.setHasFixedSize(true);
//        dashBoardAdapter = new DashBoardAdapter(this, mainList);
//        recycler_main.setAdapter(dashBoardAdapter);
//        dashBoardAdapter.setOnItemClickListner(new MyClickListner() {
//            @Override
//            public void onItemClick(int position, View view) {
//
//
//            }
//        });
//    }
//
//
//    private void selectItem(final int position, View view) {
//        switch (view.getId()) {
//            case R.id.sectionNameTextView:
////                if (!examCatagory.equalsIgnoreCase(AppConstant.Exam.DSSSB) || !examCatagory.contains(AppConstant.Exam.DSSSB)) {
////                    ArrayList<QuestionSetObject> questionSetObjects = db.getQuestionWithSectionId(setSectionId,firstLanguageId);
////                    questionTotalCount = questionSetObjects.get(position).getTotalQuestion();
//                if (isSectionPosition) {
//                    recyclerViewDrawer.findViewHolderForAdapterPosition(0).itemView.findViewById(R.id.llSectionList).setBackgroundColor(getResources().getColor(R.color.white));
//                    recyclerViewDrawer.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.llSectionList).setBackgroundColor(getResources().getColor(R.color.lightgrey));
//                    isSectionPosition = false;
//                } else if (sectionPosition == position) {
//                    recyclerViewDrawer.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.llSectionList).setBackgroundColor(getResources().getColor(R.color.lightgrey));
//                } else {
//                    recyclerViewDrawer.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.llSectionList).setBackgroundColor(getResources().getColor(R.color.lightgrey));
//                    recyclerViewDrawer.findViewHolderForAdapterPosition(sectionPosition).itemView.findViewById(R.id.llSectionList).setBackgroundColor(getResources().getColor(R.color.white));
//                }
//                sectionPosition = position;
//                questionSetBySection(position);
//                //  sectionName = sectionList.get(position).getSection_name();
////                    textViewSectionName.setText(sectionName);
//                int newSec = sectionPosition + 1;
//                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
////                        textViewSectionName.setText(Html.fromHtml("Section "+newSec+" of "+sectionList.size()+"     "+sectionName,Html.FROM_HTML_MODE_LEGACY));
//                    //textViewSectionName.setText(Html.fromHtml("Section : "+sectionName,Html.FROM_HTML_MODE_LEGACY));
//                    // tvDrawer.setText(Html.fromHtml("Section : "+sectionName,Html.FROM_HTML_MODE_LEGACY));
//                } else {
////                        textViewSectionName.setText(Html.fromHtml("Section "+newSec+" of "+sectionList.size()+"     "+sectionName));
//                    //  textViewSectionName.setText(Html.fromHtml("Section : "+sectionName));
//                    //  tvDrawer.setText(Html.fromHtml("Section : "+sectionName));
//                }
////                }
//                break;
//        }
//    }
//
//    private void questionSetBySection(int position) {
//        //  questionIndex = 0;
//        //  resetAll();
//        //   questionList = db.getQuestionWithSectionId(String.valueOf(sectionList.get(position).getId()), setLanguageId);
//        //  newQuestionList = db.getQuestionWithSectionId(String.valueOf(sectionList.get(position).getId()), setLanguageId);
//        //setSectionId = String.valueOf(sectionList.get(position).getId());
//        // loadQuestion(questionIndex);
//        drawer_layout.closeDrawers();
//    }
//
////    private void questionSetBySectionPrevious(int position){
////        resetAll();
////        questionList = db.getQuestionWithSectionId(String.valueOf(sectionList.get(position).getId()), setLanguageId);
////        newQuestionList = db.getQuestionWithSectionId(String.valueOf(sectionList.get(position).getId()), setLanguageId);
////        setSectionId = String.valueOf(sectionList.get(position).getId());
////        questionIndex = questionList.size()-1;
////        loadQuestion(questionIndex);
////        drawer_layout.closeDrawers();
////    }
//
//    //    private void drawerLayoutButton(){
////
////        ArrayList<QuestionSetObject> questionSetObjects = db.getQuestionWithSectionId(setSectionId,firstLanguageId);
////        if (questionSetObjects != null ) {
////            for (int i=0;i<questionSetObjects.size();i++){
////                String userSelect  = db.getAnswerByQuestionId(String.valueOf(questionSetObjects.get(i).getQuestion_id()));
////                Button btnRecyclerViewDrawerRight = ((recyclerViewDrawerRight).findViewHolderForAdapterPosition(i).itemView.findViewById(R.id.btnNumber));
////
////                int total = questionSetObjects.get(i).getTotalQuestion() ;
////                btnRecyclerViewDrawerRight.setText(String.valueOf(total));
////                if (userSelect != null ) {
////                    utilManager.setCircle(getResources().getColor(R.color.mediumseagreen),btnRecyclerViewDrawerRight);
////                } else {
//////
////                    utilManager.setCircle(getResources().getColor(R.color.red),btnRecyclerViewDrawerRight);
////                }
////            }
////        }
//    @Override
//    public void onClick(@Nullable View p0) {
//        super.onClick(p0);
//        String currentDate;
//
//        switch (p0.getId()) {
//            case R.id.barTextView:
//                drawer_layout.openDrawer(Gravity.START);
//                rlDrawer.setVisibility(View.GONE);
//                hideSoftKeyboard();
//                break;
//            case R.id.barTextViewRightSide:
//                rlDrawer.setVisibility(View.VISIBLE);
//                drawer_layout.openDrawer(Gravity.RIGHT);
////                drawer_layout.openDrawer(Gravity.START);
////                drawer_layout.openDrawer(Gravity.RIGHT);
////                drawer_layout.openDrawer(GravityCompat.END);
////                llDrawerHeader.setVisibility(View.GONE);
//                // drawerLayoutButton();
//                hideSoftKeyboard();
//                break;
//
//
//        }
//    }
//
//
//    private void initResponse() {
//        this.apiCallBack = new ApiCallBack() {
//            @Override
//            public void callBackResponse(String result, int requestCode) {
//                switch (requestCode) {
//                    case 1:
//                        dissmissProgress();
//                        if (result != null && !result.equals("")) {
//                            try {
//                                Log.d("result dashboard", result);
//                                JSONArray jsonArray = new JSONArray(result);
//                                for (int i = 0; i < jsonArray.length(); i++) {
//                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
//                                    String status = jsonObject.getString("status");
//                                    if (status.equalsIgnoreCase("1")) {
//                                        DashBoardObject o = new DashBoardObject(
//                                                jsonObject.getString("feedsno"),
//                                                jsonObject.getString("enduserid"),
//                                                jsonObject.getString("endownerid"),
//                                                jsonObject.getString("dtype"),
//                                                jsonObject.getString("app_title"),
//                                                jsonObject.getString("other"),
//                                                jsonObject.getString("mouse_clicks"),
//                                                jsonObject.getString("wordcount"),
//                                                jsonObject.getString("linecount"),
//                                                jsonObject.getString("decdata"),
//                                                jsonObject.getString("delete"),
//                                                jsonObject.getString("ttype")
//                                        );
//                                        mainList.add(o);
//                                        //  prefencesManager.saveString(AppConstant.PreferenceKey.USER_NAME, name);
//                                    } else {
//                                        Toast.makeText(DashBoardActivity.this, "Pleaase check User Name and Passwod", Toast.LENGTH_SHORT).show();
//                                    }
//                                    dashBoardAdapter = new DashBoardAdapter(DashBoardActivity.this, mainList);
//                                    recycler_main.setAdapter(dashBoardAdapter);
//                                }
//
//                                //startActivity(new Intent(DashBoardActivity.this, DashBoardActivity.class));
//                                //  SessionManager sessionManager = new SessionManager(DashBoardActivity.this);
//                                // sessionManager.createLoginSession(prefencesManager.getString(AppConstant.PreferenceKey.USER_NAME), prefencesManager.getString(AppConstant.PreferenceKey.EMAIL));
//                            } catch (Exception ex) {
//                                ex.printStackTrace();
//                                Toast.makeText(getApplicationContext(), "some error...", Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                        break;
//
//                }
//            }
//
//            @Override
//            public void callBackError(final Exception exception, final int requestCode) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        dissmissProgress();
//                        throwExceptions(exception, activity_candidate_Dashboard);
//                        System.out.println("result" + exception);
//                    }
//                });
//            }
//        };
//    }
//}
//

package endetect.com.endetect.activity;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import endetect.com.endetect.Fragments.ChangePasswordFragment;
import endetect.com.endetect.Fragments.DashBoardFragment;
import endetect.com.endetect.Fragments.EndUserFragment;
import endetect.com.endetect.R;
import endetect.com.endetect.RecylerObject.DashBoardObject;
import endetect.com.endetect.RecylerObject.EndUserObject;
import endetect.com.endetect.RecylerObject.SectionListObject;
import endetect.com.endetect.adapter.EndUserAdapter;
import endetect.com.endetect.adapter.DashBoardAdapter;
import endetect.com.endetect.adapter.SectionListAdapter;
import endetect.com.endetect.utity.AllAsyncTaskRequest;
import endetect.com.endetect.utity.ApiCallBack;
import endetect.com.endetect.utity.AppConstant;
import endetect.com.endetect.utity.MyClickListner;
import endetect.com.endetect.utity.SessionManager;
import in.ddinfotech.shiksha.Util.PrefencesManager;

public class MainActivity extends BaseActivity {
    DrawerLayout drawer_layout;
    RelativeLayout rlDrawer;
    SectionListAdapter adapter;
    // MyClickListner myClickListner;
    boolean isSectionPosition = true;
    int sectionPosition = 0, newSectionPosition = 0;
    DashBoardAdapter dashBoardAdapter;
    EndUserAdapter adapter2;
    Typeface typeface;
    ImageView barTextView, barTextViewRightSide;
    ArrayList<SectionListObject> sectionList = new ArrayList<>();
    ArrayList<DashBoardObject> mainList = new ArrayList<>();
    ArrayList<EndUserObject> resultsFilter = new ArrayList<>();
    ArrayList<EndUserObject> endUserObjectsList = new ArrayList<>();
    //ArrayList<QuestionSetObject> newQuestionList = new ArrayList<>();
    RecyclerView recyclerViewDrawer, recycler_main, recyclerViewDrawerRight;
    ApiCallBack apiCallBack;
    PrefencesManager prefencesManager;
    CoordinatorLayout activity_candidate_Dashboard;
    SessionManager sessionManager;
    FrameLayout frame_layout;
    TextView tvGreenSignal, tvYellowSignal, tvRedSignal;
    SearchView svAlertDropDown;
    private final static String TAG_FRAGMENT = "TAG_FRAGMENT";
    private static boolean isBackHome = false;
    private static final int TIME_DELAY = 3000;
    private static long back_pressed;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        typeface = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
        setContentView(R.layout.activity_main);
        initView();
        initResponse();
        initRecyclerViewRightSide();
        initRecyclerView();
        loadView();
        loadData();


    }


    public void initView() {
        sessionManager = new SessionManager(MainActivity.this);
        prefencesManager = new PrefencesManager(MainActivity.this);
        drawer_layout = findViewById(R.id.drawer_layout);
        frame_layout = findViewById(R.id.frame_layout);
        activity_candidate_Dashboard = findViewById(R.id.activity_candidate_Dashboard);
        rlDrawer = findViewById(R.id.rlDrawer);
        barTextView = findViewById(R.id.barTextView);
        barTextViewRightSide = findViewById(R.id.barTextViewRightSide);
        recyclerViewDrawer = findViewById(R.id.recyclerViewDrawer);
        tvGreenSignal = findViewById(R.id.tvGreenSignal);
        tvRedSignal = findViewById(R.id.tvRedSignal);
        tvYellowSignal = findViewById(R.id.tvYellowSignal);
        tvYellowSignal.setTypeface(typeface);
        tvRedSignal.setTypeface(typeface);
        tvGreenSignal.setTypeface(typeface);
        tvGreenSignal.setText(getResources().getString(R.string.circle_icon));
        tvYellowSignal.setText(getResources().getString(R.string.circle_icon));
        tvRedSignal.setText(getResources().getString(R.string.circle_icon));
        tvGreenSignal.setTextColor(getResources().getColor(R.color.green));
        tvYellowSignal.setTextColor(getResources().getColor(R.color.yellow));
        tvRedSignal.setTextColor(getResources().getColor(R.color.red));
//        recycler_main = findViewById(R.id.rv_main);
        recyclerViewDrawerRight = findViewById(R.id.recyclerViewDrawerRight);
        setOnClickListener(R.id.barTextView, R.id.barTextViewRightSide);
        svAlertDropDown = findViewById(R.id.svAlertDropDown);
        svAlertDropDown.setIconifiedByDefault(false);
        svAlertDropDown.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filter(newText);
                return false;
            }
        });
    }

    private void loadView() {
        isBackHome = false;
        final DashBoardFragment fragment = new DashBoardFragment();
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, fragment, TAG_FRAGMENT);
        transaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        (adapter).setOnItemClickListner(new MyClickListner() {
            @Override
            public void onItemClick(int position, View view) {
                if (position == 4) {
                    sessionManager.logoutUser();
                } else if ((position == 3)) {
                    isBackHome = true;
                    final ChangePasswordFragment fragment = new ChangePasswordFragment();
                    final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame_layout, fragment, TAG_FRAGMENT);
                    transaction.addToBackStack(null);
                    transaction.commit();
                } else if ((position == 1))
                {
                    loadView();
                } else if (position == 0) {
                    loadView();
                }


                if (isSectionPosition) {
                    recyclerViewDrawer.findViewHolderForAdapterPosition(0).itemView.findViewById(R.id.llSectionList).setBackgroundColor(getResources().getColor(R.color.white));
                    recyclerViewDrawer.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.llSectionList).setBackgroundColor(getResources().getColor(R.color.grey));
                    isSectionPosition = false;
                } else if (sectionPosition == position) {
                    recyclerViewDrawer.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.llSectionList).setBackgroundColor(getResources().getColor(R.color.grey));
                } else {
                    recyclerViewDrawer.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.llSectionList).setBackgroundColor(getResources().getColor(R.color.grey));
                    recyclerViewDrawer.findViewHolderForAdapterPosition(sectionPosition).itemView.findViewById(R.id.llSectionList).setBackgroundColor(getResources().getColor(R.color.white));
                }
                sectionPosition = position;


                drawer_layout.closeDrawers();
            }
        });
        (adapter2).setOnItemClickListner(new MyClickListner() {
            @Override
            public void onItemClick(int position, View view) {
                isBackHome = true;
                String id =endUserObjectsList.get(position).getSno();
                Bundle bundle = new Bundle();
                bundle.putString("pageNo", prefencesManager.getString(AppConstant.PreferenceKey.pageNum));
                bundle.putString(AppConstant.PreferenceKey.FEEDNO, prefencesManager.getString(AppConstant.PreferenceKey.FEEDNO));
                bundle.putString("ENDUSERID",id);
                DashBoardFragment dashBoardFragment = new DashBoardFragment();
                dashBoardFragment.setArguments(bundle);
                final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, dashBoardFragment, TAG_FRAGMENT);
                transaction.addToBackStack(null);
                transaction.commit();
                drawer_layout.closeDrawers();


            }
        });
    }


    private void initRecyclerView() {
        SectionListObject o = new SectionListObject(getResources().getDrawable(R.drawable.dashboard_activity), "DashBoard/Activity");
        SectionListObject o1 = new SectionListObject(getResources().getDrawable(R.drawable.ref), "Live");
        SectionListObject o2 = new SectionListObject(getResources().getDrawable(R.drawable.manage_user), "Manager User");
        SectionListObject o3 = new SectionListObject(getResources().getDrawable(R.drawable.change_password), "Change Password");
        SectionListObject o4 = new SectionListObject(getResources().getDrawable(R.drawable.candidate_pic), "Logout");
        sectionList.add(o);
        sectionList.add(o1);
        sectionList.add(o2);
        sectionList.add(o3);
        sectionList.add(o4);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerViewDrawer.setLayoutManager(manager);
        recyclerViewDrawer.setHasFixedSize(true);
        adapter = new SectionListAdapter(this, sectionList);
        recyclerViewDrawer.setAdapter(adapter);
    }

    private void initRecyclerViewRightSide() {
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerViewDrawerRight.setLayoutManager(manager);
        recyclerViewDrawerRight.setHasFixedSize(true);
        adapter2 = new EndUserAdapter(this, endUserObjectsList);
        recyclerViewDrawerRight.setAdapter(adapter2);
    }


    @Override
    public void onClick(@Nullable View p0) {
        super.onClick(p0);
        String currentDate;

        switch (p0.getId()) {
            case R.id.barTextView:
                drawer_layout.openDrawer(Gravity.START);
                rlDrawer.setVisibility(View.GONE);
                hideSoftKeyboard();
                break;
            case R.id.barTextViewRightSide:
                rlDrawer.setVisibility(View.VISIBLE);
                drawer_layout.openDrawer(Gravity.RIGHT);
                hideSoftKeyboard();
                break;


        }
    }


    private void initResponse() {
        this.apiCallBack = new ApiCallBack() {
            @Override
            public void callBackResponse(String result, int requestCode) {
                switch (requestCode) {
                    case 1:
                        dissmissProgress();
                        if (result != null && !result.equals("")) {
                            try {
                                Log.d("result enduser", result);
                                JSONArray jsonArray = new JSONArray(result);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    EndUserObject o = new EndUserObject(
                                            jsonObject.getString("sno"),
                                            jsonObject.getString("name"),
                                            jsonObject.getString("active"),
                                            jsonObject.getString("pcname"),
                                            jsonObject.getString("startuptime"),
                                            jsonObject.getString("shutdowntime"),
                                            jsonObject.getString("lastaccesstime"),
                                            jsonObject.getString("profilepic"),
                                            jsonObject.getString("ipaddress"));
                                    endUserObjectsList.add(o);
                                    resultsFilter.add(o);
                                    if (i == jsonArray.length() - 1) {
                                        String enduserid = jsonObject.getString("sno");
                                        prefencesManager.saveString(AppConstant.PreferenceKey.ENDUSERID, enduserid);
                                    }
                                }
                                adapter2.notifyDataSetChanged();
                            } catch (Exception ex) {
                                dissmissProgress();
                                ex.printStackTrace();
                                Toast.makeText(getApplicationContext(), "some error...", Toast.LENGTH_SHORT).show();
                            }
                        }
                        break;

                }
            }

            @Override
            public void callBackError(final Exception exception, final int requestCode) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dissmissProgress();
                        throwExceptions(exception, activity_candidate_Dashboard);
                        System.out.println("result" + exception);
                    }
                });
            }
        };
    }

    private void loadData() {
        showProgress();
        new AllAsyncTaskRequest(MainActivity.this, apiCallBack).setListener(AppConstant.ApiCall.GET_ENDUSERS, 1).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, prefencesManager.getString(AppConstant.PreferenceKey.QNWER_ID));
    }


    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        endUserObjectsList.clear();
        if (charText.length() == 0) {
            endUserObjectsList.addAll(resultsFilter);
            hideSoftKeyboard();
        } else {
            for (EndUserObject wp : resultsFilter) {
                if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    endUserObjectsList.add(wp);
                }
            }
        }
        adapter2.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START) || rlDrawer.getVisibility() == View.VISIBLE) {
            drawer_layout.closeDrawers();
            rlDrawer.setVisibility(View.GONE);
        } else if (isBackHome) {
            loadView();
            isBackHome = false;
            drawer_layout.closeDrawers();
        } else {
            if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
                super.onBackPressed();
            } else {
                Toast.makeText(getBaseContext(), "Press once again to exit!",
                        Toast.LENGTH_SHORT).show();
            }
            back_pressed = System.currentTimeMillis();
        }
    }
}


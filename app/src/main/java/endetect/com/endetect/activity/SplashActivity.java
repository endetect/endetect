package endetect.com.endetect.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import endetect.com.endetect.R;
import endetect.com.endetect.utity.SessionManager;


import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;


public class SplashActivity extends AppCompatActivity {
    private static final long SPLASH_DISPLAY_LENGTH = 3000;
    private String imei, auto_time_off, bluetoothID;
    Intent mServiceIntent;
    private TelephonyManager tel;
    Context context;

    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = this;

        sessionManager = new SessionManager(SplashActivity.this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */

                if (sessionManager.isLoggedIn()) {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                } else {
                   startActivity(new Intent(SplashActivity.this, SlideActivity.class));
                    finish();

                }

            }
        }, SPLASH_DISPLAY_LENGTH);


    }


    @Override
    protected void onDestroy() {

        super.onDestroy();

    }
}

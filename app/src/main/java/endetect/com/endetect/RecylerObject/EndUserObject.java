package endetect.com.endetect.RecylerObject;

public class EndUserObject {
    String sno;
    String name;
    String active;
    String pcName;
    String startuptime;
    String shutdowntime;
    String lastaccesstime;
    String profilepic;
    String ipaddress;

    public EndUserObject(){}

    public EndUserObject(String sno, String name, String active, String pcName, String startuptime, String shutdowntime, String lastaccesstime, String profilepic, String ipaddress) {
        this.sno = sno;
        this.name = name;
        this.active = active;
        this.pcName = pcName;
        this.startuptime = startuptime;
        this.shutdowntime = shutdowntime;
        this.lastaccesstime = lastaccesstime;
        this.profilepic = profilepic;
        this.ipaddress = ipaddress;
    }

    public String getSno() {
        return sno;
    }

    public String getName() {
        return name;
    }

    public String getActive() {
        return active;
    }

    public String getPcName() {
        return pcName;
    }

    public String getStartuptime() {
        return startuptime;
    }

    public String getShutdowntime() {
        return shutdowntime;
    }

    public String getLastaccesstime() {
        return lastaccesstime;
    }

    public String getProfilepic() {
        return profilepic;
    }

    public String getIpaddress() {
        return ipaddress;
    }
}

package endetect.com.endetect.RecylerObject;

import android.graphics.drawable.Drawable;

public class DashBoardObject {
    String id,title,last_seen,activity;
    String dtype;
    String feedsno;
    String enduserid;
    String endownerid;
    String app_title;
    String other;
    String mouse_clicks;
    String wordcount;
    String linecount;
    String decdata;
    String delete;
    String ttype;
    String iv_iv_activity_sc,iv_profile_image;

    public DashBoardObject() {

    }

    public DashBoardObject(
                           String feedsno,
                           String enduserid,
                           String endownerid,
                           String dtype,
                           String app_title,
                           String other,
                           String mouse_clicks,
                           String wordcount,
                           String linecount,
                           String decdata,
                           String delete,
                           String ttype)


    {
this.feedsno=feedsno;
this.enduserid=enduserid;
this.endownerid=endownerid;
this.dtype=dtype;
this.app_title=app_title;
this.other=other;
this.mouse_clicks=mouse_clicks;
this.wordcount=wordcount;
this.linecount=linecount;
this.decdata=decdata;
this.delete=delete;
this.ttype=ttype;
this.iv_iv_activity_sc=iv_iv_activity_sc;
this.iv_profile_image=iv_profile_image;



    }
    public String getDtype() {
        return dtype;
    }

    public String getFeedsno() {
        return feedsno;
    }

    public String getEnduserid() {
        return enduserid;
    }

    public String getEndownerid() {
        return endownerid;
    }

    public String getApp_title() {
        return app_title;
    }

    public String getOther() {
        return other;
    }

    public String getMouse_clicks() {
        return mouse_clicks;
    }

    public String getWordcount() {
        return wordcount;
    }

    public String getLinecount() {
        return linecount;
    }

    public String getDecdata() {
        return decdata;
    }

    public String getDelete() {
        return delete;
    }

    public String getTtype() {
        return ttype;
    }


















    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLast_seen(String last_seen) {
        this.last_seen = last_seen;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public void setIv_iv_activity_sc(String iv_iv_activity_sc) {
        this.iv_iv_activity_sc = iv_iv_activity_sc;
    }

    public void setIv_profile_image(String iv_profile_image) {
        this.iv_profile_image = iv_profile_image;
    }

    public String getId() {

        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getLast_seen() {
        return last_seen;
    }

    public String getActivity() {
        return activity;
    }

    public String getIv_iv_activity_sc() {
        return iv_iv_activity_sc;
    }

    public String getIv_profile_image() {
        return iv_profile_image;
    }
}

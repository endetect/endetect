package endetect.com.endetect.RecylerObject;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

public class SectionListObject implements Parcelable {

    int id;

    String section_name;
    String section_type;
    int no_of_questions;
    Drawable imageView;
    int Time_Duration;

//    public SectionListObject(int id, String section_name,
//                             String section_type,
//                             int no_of_questions,
//                             String exam_code,
//                             int time_Duration) {
//        this.id = id;
//        this.section_name = section_name;
//        this.section_type = section_type;
//        this.no_of_questions = no_of_questions;
//        this.exam_code = exam_code;
//        Time_Duration = time_Duration;
//    }
    public SectionListObject(Drawable imageView, String section_name
                             ) {
       // this.id = id;
        this.section_name = section_name;
        this.imageView = imageView;

    }

    protected SectionListObject(Parcel in) {
        id = in.readInt();
        section_name = in.readString();
        section_type = in.readString();
        no_of_questions = in.readInt();
//        exam_code = in.readString();
//        Time_Duration = in.readInt();
    }

    public int getId() {
        return id;
    }

    public String getSection_name() {
        return section_name;
    }

    public String getSection_type() {
        return section_type;
    }

    public int getNo_of_questions() {
        return no_of_questions;
    }

    public Drawable getImage() {
        return imageView;
    }

    public int getTime_Duration() {
        return Time_Duration;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(section_name);
        parcel.writeString(section_type);
        parcel.writeInt(no_of_questions);
       // parcel.writeBi(imageView);
        parcel.writeInt(Time_Duration);
    }

    public static final Creator<SectionListObject> CREATOR = new Creator<SectionListObject>() {
        @Override
        public SectionListObject createFromParcel(Parcel in) {
            return new SectionListObject(in);
        }

        @Override
        public SectionListObject[] newArray(int size) {
            return new SectionListObject[size];
        }
    };
}

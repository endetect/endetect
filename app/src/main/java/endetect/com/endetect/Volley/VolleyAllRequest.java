package endetect.com.endetect.Volley;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mahendra on 11/06/2018.
 */

public class VolleyAllRequest {
    String tag_json_obj = "json_obj_req";
    String jsonArrayResponse = null;
    Context context;

    public VolleyAllRequest(Context context){
        this.context = context;
    }

    static String urlEncodeUTF8(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedOperationException(e);
        }
    }
    static String urlEncodeUTF8(Map<?,?> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<?,?> entry : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(String.format("%s=%s",
                    urlEncodeUTF8(entry.getKey().toString()),
                    urlEncodeUTF8(entry.getValue().toString())
            ));
        }
        return sb.toString();
    }

    public void jsonObjectRequestGetMethod(String url, final HashMap<String,Object> params, final android.os.Handler handler, final int taskCode) {
        if(params!=null) {
            url = url + "?" + urlEncodeUTF8(params);
        }
        JsonObjectRequest jsonObjReq = null;
        try {
           // Log.d("VolleyUrl ", url);
            //Log.d("Params",params.toString());
            jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Bundle bundle1 = new Bundle();
                            Message msg = new Message();
                            msg.what = taskCode;
                            bundle1.putInt("taskCode",taskCode);
                            bundle1.putString("response", response.toString());
                            msg.setData(bundle1); //put the data here
                            handler.sendMessage(msg);
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
               //     Log.d("error", error.toString());
                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null && networkResponse.statusCode == 401) {
                        Log.d("Authorization","Unauthorized user");
                    }
                }
            });
//            {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> headers = new HashMap<String, String>();
//                    headers.put("Content-Type", "application/json");
//                    headers.put(AppConstant.PREF_KEYS.AUTHORIZAION, Preferences.getData(AppConstant.PREF_KEYS.TOKEN_TYPE, "") + " " + Preferences.getData(AppConstant.PREF_KEYS.TOKEN, ""));
//                    return headers;
//                }
//
//            };
//            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
//                    500000,5,
//                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
//            ));
        } catch (Exception e) {
            e.printStackTrace();
        }

        AppController.getInstance(context).addToRequestQueue(jsonObjReq);
// Adding request to request queue
//        AppController.getInstance(context).addToRequestQueue(jsonObjReq);
//        RequestQueue requestQueue = Volley.newRequestQueue(context);
//        requestQueue.add(jsonObjReq);
    }


    public void jsonObjectRequestGetMethod(String url, final HashMap<String,Object> params, final int taskCode,final VolleyCallback callback) {
        if(params!=null) {
            url = url + "?" + urlEncodeUTF8(params);
        }
        JsonObjectRequest jsonObjReq = null;
        try {
            // Log.d("VolleyUrl ", url);
            //Log.d("Params",params.toString());
            jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            callback.onSuccessResponse(response.toString());
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    callback.onErrorResponse(error);
                }
            });
//            {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> headers = new HashMap<String, String>();
//                    headers.put("Content-Type", "application/json");
//                    headers.put(AppConstant.PREF_KEYS.AUTHORIZAION, Preferences.getData(AppConstant.PREF_KEYS.TOKEN_TYPE, "") + " " + Preferences.getData(AppConstant.PREF_KEYS.TOKEN, ""));
//                    return headers;
//                }
//
//            };
//            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
//                    100000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
//            ));
        } catch (Exception e) {
            e.printStackTrace();
        }

        AppController.getInstance(context).addToRequestQueue(jsonObjReq);
//        RequestQueue requestQueue = Volley.newRequestQueue(context);
//        requestQueue.add(jsonObjReq);
    }



    public void jsonObjectRequestPostMethod(String url, final String jsonBody,final VolleyCallback callback) throws JSONException {
//        if(params!=null) {
//            url = url + "?" + urlEncodeUTF8(params);
//        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callback.onSuccessResponse(response.toString());
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onErrorResponse(error);
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return jsonBody == null ? null : jsonBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", jsonBody, "utf-8");
                    return null;
                }
            }

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//               // headers.put("Content-Type", "application/json");
//                headers.put(AppConstant.PREF_KEYS.AUTHORIZAION, Preferences.getData(AppConstant.PREF_KEYS.TOKEN_TYPE, "") + " " + Preferences.getData(AppConstant.PREF_KEYS.TOKEN, ""));
//                return headers;
//            }

        };

// Adding request to request queue
       // AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
//        RequestQueue requestQueue = Volley.newRequestQueue(context);
//        //adding the string request to request queue
//        requestQueue.add(jsonObjReq);
        AppController.getInstance(context).addToRequestQueue(jsonObjReq);
    }

    public void jsonArrayRequest(String url, final String jsonBody,final VolleyCallback callback) throws JSONException {
//        if(params!=null) {
//            url = url + "?" + urlEncodeUTF8(params);
//        }
        JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.POST,
                url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        callback.onSuccessResponse(response.toString());
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onErrorResponse(error);
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return jsonBody == null ? null : jsonBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", jsonBody, "utf-8");
                    return null;
                }
            }

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//               // headers.put("Content-Type", "application/json");
//                headers.put(AppConstant.PREF_KEYS.AUTHORIZAION, Preferences.getData(AppConstant.PREF_KEYS.TOKEN_TYPE, "") + " " + Preferences.getData(AppConstant.PREF_KEYS.TOKEN, ""));
//                return headers;
//            }

        };

// Adding request to request queue
        // AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
//        RequestQueue requestQueue = Volley.newRequestQueue(context);
//        //adding the string request to request queue
//        requestQueue.add(jsonObjReq);
        AppController.getInstance(context).addToRequestQueue(jsonObjReq);
    }


    public void   jsonArrayRequest(String url, final String  params, final android.os.Handler handler, final int taskCode){

        String tag_json_arry = "json_array_req";
        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //    Log.d(TAG, response.toString());
                        // pDialog.hide();
                        jsonArrayResponse = response.toString();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  VolleyLog.d(TAG, "Error: " + error.getMessage());
                // pDialog.hide();
                jsonArrayResponse = "some error";
            }
        });

// Adding request to request queue
      //   AppController.getInstance(context).addToRequestQueue(req);

    }


    public void volleyPostRequest(String url,  final VolleyCallback callback){


          //  url = url + "?" + urlEncodeUTF8(params);


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(context, "Response "+response.toString(), Toast.LENGTH_SHORT).show();
                        callback.onSuccessResponse(response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "something went wrong"+error, Toast.LENGTH_SHORT).show();
                callback.onErrorResponse(error);
            }
        })

        {

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                headers.put("X-API-KEY" , "A1F584C3083132CE18DCDA579C753579D3276AAB");
//                return headers;
//            }


//            @Override
//            protected Map<String, String> getParams() {
//            Map<String, String> params = new HashMap<String, String>();
//            params.put("username","pavit.cellgell@gmail.com");
//            params.put("password","&^RFCUI+LKHG@$");
//            params.put("deviceid","abc");
//            return params;
//
//        }
//
//    {

        //This is for Headers If You Needed
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> params = new HashMap<String, String>();
       // params.put("Content-Type", "application/json; charset=UTF-8");
        params.put("X-API-KEY" , "A1F584C3083132CE18DCDA579C753579D3276AAB");
        return params;
    }

        //Pass Your Parameters here
        @Override
        protected Map<String, String> getParams() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("username","pavit.cellgell@gmail.com");
        params.put("password","&^RFCUI+LKHG@$");
        params.put("deviceid","abc");
        return params;
    }

            };
        AppController.getInstance(context).addToRequestQueue(stringRequest);
    }


    public void stringRequestPost(String url, final String  body){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(context, "Response "+response.toString(), Toast.LENGTH_SHORT).show();
                        // valid response
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error
                Log.d("Error",error.toString());
                Toast.makeText(context, "something went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {
                @Override
                public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
                @Override
                public byte[] getBody() {
                try {
                    return body == null ? null : body.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", body, "utf-8");
                    return null;
                }
            }
        };
//        {

//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("userId",Preferences.getData(AppConstant.PREF_KEYS.USER_ID, ""));
//                params.put("ticketDetailId",para);
//                return params;
//
//            }

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                headers.put(AppConstant.PREF_KEYS.AUTHORIZAION, Preferences.getData(AppConstant.PREF_KEYS.TOKEN_TYPE, "") + " " + Preferences.getData(AppConstant.PREF_KEYS.TOKEN, ""));
//                return headers;
//            }
//        };
        AppController.getInstance(context).addToRequestQueue(stringRequest);

    }
}

package endetect.com.endetect.Volley;

import com.android.volley.VolleyError;

/**
 * Created by dell on 4/6/2018.
 */

public interface VolleyCallback {
    void onSuccessResponse(String response);
    void onErrorResponse(VolleyError error);
    String a = "";
}

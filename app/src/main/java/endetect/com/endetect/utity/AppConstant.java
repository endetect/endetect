package endetect.com.endetect.utity;

public class AppConstant {
    public static final String SHARED_PREF = "SHARED_PREF";
    public static final String IP_ADDRESS = "IP_ADDRESS";

    public interface ApiCall{
        String CANDIDATE_LOGIN = "login/";
        String GET_FEEDS = "getfeeds/";
        String GET_ENDUSERS = "getendusers/";
        String CHANGE_PASSWORD = "changepassword/";
        }

    public interface PreferenceKey {
        String EMAIL = "EMAIL";
        String DEVICE_ID = "device_id";
        String MOBILE = "MOBILE";
        String QNWER_ID = "EXAM_NAME";
        String TIMER = "TIMER";
        String USER_NAME = "USER_NAME";
        String ENDUSERID = "ENDUSERID";
        String FEEDNO = "feedsno";
        String pageNum = "pageNo";
    }
}

package endetect.com.endetect.utity;

/**
 * Created by Mahendra singh gusain on 11-06-2018.
 */
public interface ApiCallBack {
    void callBackResponse(String result, int requestCode);
    void callBackError(Exception exception, int requestCode);
}

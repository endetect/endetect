package endetect.com.endetect.utity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

import endetect.com.endetect.R;
import in.ddinfotech.shiksha.Util.PrefencesManager;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by Mahendra singh gusain on 11-06-2018.
 */

public class UtilManager {
    Context context;
    private static final String TAG = "AirplaneModeActivity";
    private final String COMMAND_FLIGHT_MODE_1 = "settings put global airplane_mode_on";
    private final String COMMAND_FLIGHT_MODE_2 = "am broadcast -a android.intent.action.AIRPLANE_MODE --ez state";
    PrefencesManager prefencesManager;
    Typeface tvFontIcon;

    public UtilManager(Context context){
        this.context = context;
        prefencesManager = new PrefencesManager(context);
        tvFontIcon=Typeface.createFromAsset(context.getAssets(),"fontawesome-webfont.ttf");
    }

    public String getNetworkType() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if(info==null || !info.isConnected())
            return "-";
        if(info.getType() == ConnectivityManager.TYPE_WIFI)
            return "WIFI";
        if(info.getType() == ConnectivityManager.TYPE_MOBILE){
            int networkType = info.getSubtype();
            switch (networkType) {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN:
                    return "2G";
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_EVDO_B:
                case TelephonyManager.NETWORK_TYPE_EHRPD:
                case TelephonyManager.NETWORK_TYPE_HSPAP:
                    return "3G";
                case TelephonyManager.NETWORK_TYPE_LTE:
                    return "4G";
                default:
                    return "?";
            }
        }
        return "?";
    }

    public String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':')<0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim<0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ignored) { } // for now eat exceptions
        return "";
    }

    public String getMacAddress() {
        WifiManager wimanager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        String macAddress = wimanager.getConnectionInfo().getMacAddress();
        if (macAddress == null) {
            macAddress = "Device don't have mac address or wi-fi is disabled";
        }
        return macAddress;
    }

    public String getMacAddr() {             // For Marshmallow version
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
            //handle exception
        }
        return "";

    }


    public boolean checkPermission(int permissionValue){
        int result;
        switch (permissionValue){
            case 101:
                result = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
                if (result == PackageManager.PERMISSION_GRANTED){
                    return true;
                } else {
                    return false;
                }
            case 102:
                result = ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE);
                if (result == PackageManager.PERMISSION_GRANTED){
                    return true;
                } else {
                    return false;
                }
            case 103:
                result = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (result == PackageManager.PERMISSION_GRANTED){
                    return true;
                } else {
                    return false;
                }

            case 104:
                result = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
                if (result == PackageManager.PERMISSION_GRANTED){
                    return true;
                } else {
                    return false;
                }

            case 105:
                result = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS);
                if (result == PackageManager.PERMISSION_GRANTED){
                    return true;
                } else {
                    return false;
                }

            case 106:

                break;

        }
        return false;
    }



    public void alertPermission(String message) {
        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        builder.setTitle("Permission Required");
        builder.setMessage(message);
        builder.setPositiveButton("App Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                intent.setData(uri);
                context.startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }


    public void zoomImageWithUrl(String imageName) {
        android.support.v7.app.AlertDialog.Builder builder=new android.support.v7.app.AlertDialog.Builder(context);
        View alertView= LayoutInflater.from(context).inflate(R.layout.layout_zoom_imageview,null);
        builder.setView(alertView);
        final android.support.v7.app.AlertDialog alertDialog=builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ZoomableImageView ivAlertZoom=(ZoomableImageView)alertView.findViewById(R.id.ivAlertZoom);
        ivAlertZoom.setVisibility(View.GONE);
        ImageView ivZoomImage=(ImageView)alertView.findViewById(R.id.ivZoomImage);
        ivZoomImage.setVisibility(View.VISIBLE);
        String urlImage=ServiceApi.localUrl+imageName;
        Log.d("UtilsManager",urlImage);
        PhotoViewAttacher photoViewAttacher=new PhotoViewAttacher(ivZoomImage);
        photoViewAttacher.update();
        Picasso.with(context)
                .load(urlImage)
                .into(ivZoomImage);
        TextView tvAlertCancel= alertView.findViewById(R.id.tvAlertCancel);
        tvAlertCancel.setTypeface(tvFontIcon);
        tvAlertCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }


}

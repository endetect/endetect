package endetect.com.endetect.utity;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import in.ddinfotech.shiksha.Util.PrefencesManager;

/**
 * Created by Mahendra singh gusain on 11-06-2018.
 */
 public class ServiceApi {

    Context context;
//    public static final String localUrl = "http://192.168.0.12/";
    public static  String localUrl="http://app.endetect.com/api/ed/" ;

    private static final String TAG = ServiceApi.class.getSimpleName();
    PrefencesManager prefencesManager;

    public ServiceApi(Context context) {
        this.context = context;
        prefencesManager = new PrefencesManager(context);
    }

    /* ********************** Start Post Request ********************* */

    private String load(String contents, String requesttype) throws IOException {

        Log.d(TAG,localUrl+requesttype+contents);
        URL url = new URL(localUrl+ requesttype);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setConnectTimeout(120000);
        conn.setReadTimeout(120000);
      //  conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("X-API-KEY", "A1F584C3083132CE18DCDA579C753579D3276AAB");
        conn.setDoOutput(true);
        conn.setDoInput(true);
        OutputStreamWriter w = new OutputStreamWriter(conn.getOutputStream());
        w.write(contents);
        w.flush();
        int status = conn.getResponseCode();
        InputStream istream;
        if (status != HttpURLConnection.HTTP_OK) {
             istream = conn.getErrorStream();
        }
        else {
             istream = conn.getInputStream();
        }
     //   InputStream istream = conn.getInputStream();
        String result = convertStreamToUTF8String(istream);
        return result;
    }

    private static String convertStreamToUTF8String(InputStream stream)
            throws IOException {
        String result = "";
        StringBuilder sb = new StringBuilder();
        try {
            InputStreamReader reader = new InputStreamReader(stream, "UTF-8");
            char[] buffer = new char[4096];
            int readedChars = 0;
            while (readedChars != -1) {
                readedChars = reader.read(buffer);
                if (readedChars > 0)
                    sb.append(buffer, 0, readedChars);
            }
            result = sb.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }

    /* ********************** End Post Request ********************* */

    /* ********************** Start Get Request ********************* */

    private String loadGet(String urlWithData, String requestType) throws Exception {
        try {
            localUrl = prefencesManager.getString(AppConstant.IP_ADDRESS);
            Log.d(TAG,localUrl+urlWithData);
            URL mUrl = new URL(localUrl+urlWithData);
            HttpURLConnection httpConnection = (HttpURLConnection) mUrl.openConnection();
            httpConnection.setRequestMethod(requestType);
            httpConnection.setRequestProperty("Content-Type", "application/json");
            httpConnection.setRequestProperty("X-KeyRegToken", "v;U?F=Vh9s$xkm!2C5~L:mXn&E[5bNq~,>u?WfF;G");
            httpConnection.setUseCaches(false);
            httpConnection.setAllowUserInteraction(false);
            httpConnection.setConnectTimeout(60000);
            httpConnection.setReadTimeout(60000);
            httpConnection.connect();
            int responseCode = httpConnection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                return sb.toString();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private Uri buildURI(String url, HashMap<String, String> params) {
        localUrl = prefencesManager.getString(AppConstant.IP_ADDRESS);
        Uri.Builder builder = Uri.parse(localUrl + url).buildUpon();

        if (params != null) {
            for (HashMap.Entry<String, String> entry : params.entrySet()) {
                builder.appendQueryParameter(entry.getKey(), entry.getValue());
            }
        }
        return builder.build();
    }


    /* ********************** End Get Request ********************* */



    /* *********************** Simple Post Request ********************* */

    public String loadPost(String requestType, HashMap<String, String> data) {
        URL url;
        String response = "";
        try {
            localUrl = prefencesManager.getString(AppConstant.IP_ADDRESS);
            url = new URL(localUrl + requestType);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(60000);
            conn.setConnectTimeout(60000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(data));

            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    /* ********************** End Post Request ********************* */


    public String loginUser(String userID,String password,String deviceid) throws Exception {
        String urlParameters ="username="+userID+"&password="+password+"&deviceid="+deviceid;
        String r = load(urlParameters, AppConstant.ApiCall.CANDIDATE_LOGIN);
        return r;
    }

    public String getEndUserData(String userID) throws Exception {
        String urlParameters ="ownerid="+userID;
        String r = load(urlParameters, AppConstant.ApiCall.GET_ENDUSERS);
        return r;
    }
    public String getfeeds(String ownerid, String firstrecordid, String streamolder,String pageNo,String enduserId) throws Exception {
        String urlParameters ="ownerid="+ownerid+"&firstrecordid="+firstrecordid+"&streamolder="+streamolder+"&page="+pageNo+"&enduserid="+enduserId;
                String r = load(urlParameters, AppConstant.ApiCall.GET_FEEDS);
        return r;
    }



    public String changePassword(String ownerid,String Currentpassword ,String Password,String Confirmpassword ) throws Exception {
        String urlParameters ="ownerid="+ownerid+"&currentpassword="+Currentpassword +"&password="+Password+"&cpassword="+Confirmpassword  ;
        String r = load(urlParameters, AppConstant.ApiCall.CHANGE_PASSWORD);
        return r;
    }

}
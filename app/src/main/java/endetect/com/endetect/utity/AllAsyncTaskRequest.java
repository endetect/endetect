package endetect.com.endetect.utity;

import android.content.Context;
import android.os.AsyncTask;

/**
 * Created by Mahendra singh gusain on 11-06-2018.
 */

public class AllAsyncTaskRequest extends AsyncTask<String ,String ,String > {

        ServiceApi serviceApi ;
        ApiCallBack delegate = null;
        int requestCode;
        String url;
        String result="";
        Context context;

        public AllAsyncTaskRequest(Context context , ApiCallBack asyncResponse){
            this.delegate = asyncResponse;
            this.context = context;
            serviceApi = new ServiceApi(context);
        }

        public AllAsyncTaskRequest setListener(String url, int requestCode){
            this.url = url;
            this.requestCode = requestCode;
            return this;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                if(url.equalsIgnoreCase(AppConstant.ApiCall.CANDIDATE_LOGIN)){
                    result=serviceApi.loginUser(strings[0],strings[1],strings[2]);
                }
                else if (url.equalsIgnoreCase(AppConstant.ApiCall.GET_FEEDS)){
                    result=serviceApi.getfeeds(strings[0],strings[1],strings[2],strings[3],strings[4]);
                }
                else if (url.equalsIgnoreCase(AppConstant.ApiCall.CHANGE_PASSWORD)){
                    result=serviceApi.changePassword(strings[0],strings[1],strings[2],strings[3]);
                }else if (url.equalsIgnoreCase(AppConstant.ApiCall.GET_ENDUSERS)){
                    result = serviceApi.getEndUserData(strings[0]);
                }

            }  catch (Exception e) {
                this.delegate.callBackError(e,requestCode);
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (delegate!=null){
                    if(!result.equalsIgnoreCase("") && result != null) {
                        this.delegate.callBackResponse(result, requestCode);
                        System.out.println("result" + result);

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                this.delegate.callBackError(e,requestCode);
            }
        }
}

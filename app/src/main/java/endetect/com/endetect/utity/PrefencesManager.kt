package `in`.ddinfotech.shiksha.Util

import android.text.method.TextKeyListener.clear
import android.R.id.edit
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import endetect.com.endetect.utity.AppConstant


public class PrefencesManager {
    var context: Context
    var preferences: SharedPreferences
    var pref: SharedPreferences
    var editor: SharedPreferences.Editor
    var editorPref: SharedPreferences.Editor
    private val PREF_NAME = "endetect"

    constructor(context: Context) {
        this.context = context
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        pref = context.getSharedPreferences(AppConstant.SHARED_PREF, 0)
        editor = preferences.edit()
        editorPref = pref.edit()
    }

    fun saveBoolean(key: String, value: Boolean) {
        editor.putBoolean(key, value)
        editor.apply()
    }

    fun getBoolean(key: String): Boolean {
        return preferences.getBoolean(key, false)
    }

    fun saveString(key: String, value: String) {
        editor.putString(key, value)
        editor.apply()
    }

    fun getString(key: String): String {
        return preferences.getString(key, "")
    }

    fun clear() {
        editor.clear()
        editor.commit()
    }

}
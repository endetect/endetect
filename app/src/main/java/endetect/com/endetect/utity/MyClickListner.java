package endetect.com.endetect.utity;

import android.view.View;

/**
 * Created by Mahendra singh gusain on 21-06-2018.
 */

public interface MyClickListner {
     void onItemClick(int position, View view);
}
package endetect.com.endetect.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import endetect.com.endetect.Fragments.DashBoardFragment;
import endetect.com.endetect.R;
import endetect.com.endetect.RecylerObject.DashBoardObject;
import endetect.com.endetect.RecylerObject.SectionListObject;
import endetect.com.endetect.utity.AppConstant;
import endetect.com.endetect.utity.MyClickListner;
import in.ddinfotech.shiksha.Util.PrefencesManager;

public class DashBoardAdapter extends RecyclerView.Adapter<DashBoardAdapter.MyViewHolder> {
    private final static String TAG_FRAGMENT = "TAG_FRAGMENT";
    Context context;
    ArrayList<SectionListObject> result;
    ArrayList<DashBoardObject> mainList;
    MyClickListner myClickListner;
     PrefencesManager prefencesManager;
    public DashBoardAdapter(Context context, ArrayList<DashBoardObject> mainList) {
        this.context = context;
        this.mainList = mainList;
        prefencesManager=new PrefencesManager(context);
    }


    @NonNull
    @Override
    public DashBoardAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_row_contain_main, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final DashBoardAdapter.MyViewHolder holder, int position) {
        try {
            DashBoardObject dashBoardObject = new DashBoardObject();
            dashBoardObject = mainList.get(position);
            holder.tv_title.setText(dashBoardObject.getOther());
            holder.tv_activity.setText(dashBoardObject.getApp_title());
            holder.tv_last_seen.setText("not provided in api");
            holder.tv_decData.setText(dashBoardObject.getDecdata());
            if (dashBoardObject.getDecdata() != null && !dashBoardObject.getDecdata().equals("")) {
                Picasso.with(context)
                        .load(dashBoardObject.getDecdata())
                        .resize(50, 50)
                        .centerCrop()
                        .into(holder.iv_iv_activity_sc);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        holder.iv_drop.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (holder.spinner.getSelectedItem() == null) {
//                    holder.spinner.setVisibility(View.VISIBLE);
//                    holder.spinner.performClick();
//                }
//                Toast.makeText(context,"clik",Toast.LENGTH_SHORT).show();
//            }
//        });
//        holder.tv_title.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String id =mainList.get(holder.getAdapterPosition()).getId();
//                Bundle bundle = new Bundle();
//                bundle.putString("pageNo", prefencesManager.getString(AppConstant.PreferenceKey.pageNum));
//                bundle.putString(AppConstant.PreferenceKey.FEEDNO, prefencesManager.getString(AppConstant.PreferenceKey.FEEDNO));
//                bundle.putString("ENDUSERID",id);
//                DashBoardFragment dashBoardFragment = new DashBoardFragment();
//                dashBoardFragment.setArguments(bundle);
//                final FragmentTransaction transaction =  context.getSupportFragmentManager().beginTransaction();
//                transaction.replace(R.id.frame_layout, dashBoardFragment, "TAG_FRAGMENT");
//                transaction.addToBackStack(null);
//                transaction.commit();
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return mainList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView  iv_profile_image, iv_iv_activity_sc, iv_drop;
        TextView tv_decData,tv_title, tv_last_seen, tv_activity;
        //Spinner spinner;
        public MyViewHolder(View itemView) {
            super(itemView);
            iv_profile_image = itemView.findViewById(R.id.iv_profile_image);
            iv_iv_activity_sc = itemView.findViewById(R.id.iv_iv_activity_sc);
            iv_drop = itemView.findViewById(R.id.iv_drop);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_last_seen = itemView.findViewById(R.id.tv_last_seen);
            tv_decData = itemView.findViewById(R.id.tv_decData);
            tv_activity = itemView.findViewById(R.id.tv_activity);
            iv_profile_image.setOnClickListener(this);
            iv_drop.setOnClickListener(this);
            tv_title.setOnClickListener(this);
            iv_iv_activity_sc.setOnClickListener(this);
           // spinner = (Spinner) itemView.findViewById(R.id.sp_spinner);

        }

        @Override
        public void onClick(View view) {
            myClickListner.onItemClick(getAdapterPosition(), view);
        }
    }

    public void setOnItemClickListner(MyClickListner myClickListner) {
        this.myClickListner = myClickListner;
    }
}



package endetect.com.endetect.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import endetect.com.endetect.R;
import endetect.com.endetect.RecylerObject.SectionListObject;
import endetect.com.endetect.utity.MyClickListner;
import endetect.com.endetect.utity.SessionManager;


public class SectionListAdapter extends RecyclerView.Adapter<SectionListAdapter.MyViewHolder> {

    Context context;
    ArrayList<SectionListObject> result;
    MyClickListner myClickListner;



    public SectionListAdapter(Context context,ArrayList<SectionListObject> result){
        this.context = context;
        this.result = result;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_section_list,parent,false);
        MyViewHolder myViewHolder=new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        SectionListObject sectionListObject = result.get(position);

        holder.sectionNameTextView.setText(sectionListObject.getSection_name());
        holder.iv_image_icon.setImageDrawable(sectionListObject.getImage());
        if (position==0){
            holder.llSectionList.setBackgroundColor(context.getResources().getColor(R.color.lightgrey));
        }
        holder.llSectionList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return result.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView sectionNameTextView;
        ImageView iv_image_icon,btnLogout;
        LinearLayout llSectionList;
        public MyViewHolder(View itemView) {
            super(itemView);
            sectionNameTextView = itemView.findViewById(R.id.sectionNameTextView);
            iv_image_icon = itemView.findViewById(R.id.iv_navi_imge);
            llSectionList = itemView.findViewById(R.id.llSectionList);

            itemView.setOnClickListener(this);
            sectionNameTextView.setOnClickListener(this);
            llSectionList.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            myClickListner.onItemClick(getAdapterPosition(),view);
        }
    }
    public void setOnItemClickListner(MyClickListner myClickListner){
        this.myClickListner=myClickListner;
    }
}


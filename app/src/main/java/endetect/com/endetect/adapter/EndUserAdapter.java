package endetect.com.endetect.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
import endetect.com.endetect.R;
import endetect.com.endetect.RecylerObject.EndUserObject;
import endetect.com.endetect.utity.MyClickListner;

public class EndUserAdapter extends RecyclerView.Adapter<EndUserAdapter.MyViewHolder> {

    Context context;
    ArrayList<EndUserObject> endUserObjectArrayList;
    MyClickListner myClickListner;
    Typeface tvFontIcon;

    public EndUserAdapter(Context context, ArrayList<EndUserObject> endUserObjectArrayList){
        this.context = context;
        this.endUserObjectArrayList = endUserObjectArrayList;
        tvFontIcon= Typeface.createFromAsset(context.getAssets(),"fontawesome-webfont.ttf");
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_all_enduser,parent,false);
        MyViewHolder myViewHolder=new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        try {
            EndUserObject object = endUserObjectArrayList.get(position);
            holder.tvStartUpTime.setText(object.getStartuptime());
            holder.tvUserName.setText(object.getName());
            holder.tvSignal.setText(context.getResources().getString(R.string.circle_icon));
            if (object.getActive().equals("1")) {
                holder.tvSignal.setTextColor(context.getResources().getColor(R.color.green));
            }else {
                holder.tvSignal.setTextColor(context.getResources().getColor(R.color.red));
            }
            Picasso.with(context)
                    .load(object.getProfilepic())
                    .error(R.drawable.candidate_pic)
                    .placeholder(R.drawable.candidate_pic)
                    .into(holder.userImage);
            String startTime = object.getShutdowntime();
            String st[] = startTime.split(" ");
            String lastTime = object.getLastaccesstime();
            String lt[] = lastTime.split(" ");
            DateFormat df = new SimpleDateFormat("hh:mm:ss");
            Date date1 = df.parse(lt[1]);
            Date date2 = df.parse(st[1]);
            long diff = date2.getTime() - date1.getTime();
            long timeInSeconds = diff / 1000;
            long hours, minutes, seconds;
            if (timeInSeconds<60){
                holder.tvLastAccess.setText(String.valueOf(timeInSeconds)+"sec");
            }else if (timeInSeconds<3600){
                minutes = timeInSeconds / 60;
                holder.tvLastAccess.setText(String.valueOf(minutes)+"min");
            }else {
                hours = timeInSeconds / 3600;
                holder.tvLastAccess.setText(String.valueOf(hours)+"hr");
            }
//            hours = timeInSeconds / 3600;
//            timeInSeconds = timeInSeconds - (hours * 3600);
//            minutes = timeInSeconds / 60;
//            timeInSeconds = timeInSeconds - (minutes * 60);
//            seconds = timeInSeconds;
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return endUserObjectArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CircleImageView userImage;
        TextView tvUserName,tvStartUpTime,tvLastAccess,tvSignal;
        public MyViewHolder(View itemView) {
            super(itemView);
            userImage = itemView.findViewById(R.id.userImage);
            tvUserName = itemView.findViewById(R.id.tvUserName);
            tvStartUpTime = itemView.findViewById(R.id.tvStartUpTime);
            tvLastAccess = itemView.findViewById(R.id.tvLastAccess);
            tvSignal = itemView.findViewById(R.id.tvSignal);
            tvSignal.setTypeface(tvFontIcon);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            myClickListner.onItemClick(getAdapterPosition(),view);
        }
    }
    public void setOnItemClickListner(MyClickListner myClickListner){
        this.myClickListner=myClickListner;
    }
}



package endetect.com.endetect.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import endetect.com.endetect.R;

public class EndUserFragment extends android.support.v4.app.Fragment {

    View view;
    TextView tvUserName,tvShutDownTime,tvLastAccessTime,tvActive;
    CircleImageView imgUserImage;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_end_user,null);
        initView(view);
        initClass();
//        initRecyclerViewMain(view);
//        initResponse();
//        LoadData();
//        itemSelect();

        return view;
    }

    private void initView(View v){
        tvUserName = v.findViewById(R.id.tvUserName);
        tvShutDownTime = v.findViewById(R.id.tvShutDownTime);
        tvLastAccessTime = v.findViewById(R.id.tvLastAccessTime);
        tvActive = v.findViewById(R.id.tvActive);
        imgUserImage = v.findViewById(R.id.imgUserImage);
    }

    private void initClass(){
        Bundle args = this.getArguments();
        tvActive.setText(args.getString("active"));
        tvLastAccessTime.setText(args.getString("lastAccesstime"));
        tvShutDownTime.setText(args.getString("shutDownTime"));
        tvUserName.setText(args.getString("userName"));
        String userImage = args.getString("user_image");
        Picasso.with(getActivity())
                .load(userImage)
                .error(R.drawable.candidate_pic)
                .placeholder(R.drawable.candidate_pic)
                .into(imgUserImage);
    }
}

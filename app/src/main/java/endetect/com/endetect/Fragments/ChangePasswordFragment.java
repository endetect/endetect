package endetect.com.endetect.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import endetect.com.endetect.R;
import endetect.com.endetect.RecylerObject.DashBoardObject;
import endetect.com.endetect.activity.MainActivity;
import endetect.com.endetect.adapter.DashBoardAdapter;
import endetect.com.endetect.utity.AllAsyncTaskRequest;
import endetect.com.endetect.utity.ApiCallBack;
import endetect.com.endetect.utity.AppConstant;
import endetect.com.endetect.utity.SessionManager;
import in.ddinfotech.shiksha.Util.PrefencesManager;

public class ChangePasswordFragment extends android.support.v4.app.Fragment {
    View view;
    String Currentpassword, NewPassword, Confirmpassword;
    private Button mbForgetPassSubmit;
    private EditText new_password, confim_pssword, current_password;
    private TextInputLayout mtil_current_password, til_cofirmPassword, tilNewPassword;
    PrefencesManager prefencesManager;
    CoordinatorLayout activity_candidate_Dashboard;
    SessionManager sessionManager;
    ApiCallBack apiCallBack;
    Typeface mTypeface;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = LayoutInflater.from(getActivity()).inflate(R.layout.activity_change_password, null);

        initClass();
        initView(view);
        initResponse();

        getFontFamily();

        mbForgetPassSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidationForgetPass()) {
                    Currentpassword = current_password.getText().toString();
                    NewPassword = new_password.getText().toString();
                    Confirmpassword = confim_pssword.getText().toString();
                    //getActivity().showProgress();
                    LoadData();
                }

            }
        });
        return view;
    }
//

    private void initClass() {
        mTypeface = Typeface.createFromAsset(getActivity().getAssets(), "System San Francisco Display Regular.ttf");
        sessionManager = new SessionManager(getActivity());
        prefencesManager = new PrefencesManager(getActivity());
    }

    private void initView(View view) {
        new_password = view.findViewById(R.id.new_password);
        confim_pssword = view.findViewById(R.id.confim_pssword);
        current_password = view.findViewById(R.id.current_password);
        til_cofirmPassword = (TextInputLayout) view.findViewById(R.id.til_cofirmPassword);
        tilNewPassword = (TextInputLayout) view.findViewById(R.id.tilNewPassword);
        mtil_current_password = (TextInputLayout) view.findViewById(R.id.mtil_current_password);
        mbForgetPassSubmit = view.findViewById(R.id.mbForgetPassSubmit);
        new_password.addTextChangedListener(new MyTextWatcher(new_password));
        confim_pssword.addTextChangedListener(new MyTextWatcher(confim_pssword));
        current_password.addTextChangedListener(new MyTextWatcher(current_password));

    }

    private void LoadData() {
        new AllAsyncTaskRequest(getActivity(), apiCallBack)
                .setListener(AppConstant.ApiCall.CHANGE_PASSWORD, 1).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                prefencesManager.getString(AppConstant.PreferenceKey.QNWER_ID), Currentpassword, NewPassword, Confirmpassword);
    }

    private void getFontFamily() {
        mbForgetPassSubmit.setTypeface(mTypeface);
        new_password.setTypeface(mTypeface);
        confim_pssword.setTypeface(mTypeface);
        current_password.setTypeface(mTypeface);
    }

    private void initResponse() {
        this.apiCallBack = new ApiCallBack() {
            @Override
            public void callBackResponse(String result, int requestCode) {
                switch (requestCode) {
                    case 1:
                        //  getActivity().dissmissProgress();
                        if (result != null && !result.equals("")) {
                            try {
                                Log.wtf("result dashboard", result);
                                JSONArray jsonArray = new JSONArray(result);
                                //  for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                String status = jsonObject.getString("status");
                                String errorcode = jsonObject.getString("errorcode");
                                if (errorcode.equalsIgnoreCase("0")) {
                                    Toast.makeText(getActivity(), status, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getActivity(), status, Toast.LENGTH_SHORT).show();
                                    final Intent intent = new Intent(getActivity(), MainActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                    startActivity(intent);
                                }

                            } catch (Exception ex) {
                                ex.printStackTrace();
                                Toast.makeText(getActivity(), "some error...", Toast.LENGTH_SHORT).show();
                            }
                        }
                        break;

                }
            }

            @Override
            public void callBackError(final Exception exception, final int requestCode) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // getActivity().dissmissProgress();
                        //   getActivity().throwExceptions(exception, activity_candidate_Dashboard);

                    }
                });
                System.out.println("result" + exception);
            }
        };
    }


    private boolean checkValidationForgetPass() {
        if (!curr_validatePassword()) {
            return false;
        }
        if (!new_validatePassword()) {
            return false;
        }
        if (!confim_validatePassword()) {
            return false;
        }
        return true;

    }

    private boolean curr_validatePassword() {
        if (current_password.getText().toString().trim().isEmpty()) {
            mtil_current_password.setError(getString(R.string.err_msg_password));
            requestFocus(current_password);
            return false;
        } else {
            mtil_current_password.setError(null);
        }

        return true;
    }

    private boolean new_validatePassword() {
        if (new_password.getText().toString().trim().isEmpty()) {
            tilNewPassword.setError(getString(R.string.err_msg_password));
            requestFocus(new_password);
            return false;
        } else {
            tilNewPassword.setError(null);
        }

        return true;
    }

    private boolean confim_validatePassword() {
        if (confim_pssword.getText().toString().trim().isEmpty() || !confim_pssword.getText().toString().equals(new_password.getText().toString())) {
            til_cofirmPassword.setError(getString(R.string.confir_err_msg_password));
            requestFocus(confim_pssword);
            return false;
        } else {
            til_cofirmPassword.setError(null);
        }

        return true;
    }

    private static boolean isValidEmail(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        // return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.current_password:
                    curr_validatePassword();
                    break;
                case R.id.new_password:
                    new_validatePassword();
                    break;
                case R.id.confim_pssword:
                    confim_validatePassword();
                    break;
            }
        }
    }

}

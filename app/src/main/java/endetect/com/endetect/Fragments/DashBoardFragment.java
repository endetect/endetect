package endetect.com.endetect.Fragments;


import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import endetect.com.endetect.R;
import endetect.com.endetect.RecylerObject.DashBoardObject;
import endetect.com.endetect.RecylerObject.SectionListObject;
import endetect.com.endetect.activity.MainActivity;
import endetect.com.endetect.adapter.EndUserAdapter;
import endetect.com.endetect.adapter.DashBoardAdapter;
import endetect.com.endetect.adapter.SectionListAdapter;
import endetect.com.endetect.utity.AllAsyncTaskRequest;
import endetect.com.endetect.utity.ApiCallBack;
import endetect.com.endetect.utity.AppConstant;
import endetect.com.endetect.utity.MyClickListner;
import endetect.com.endetect.utity.SessionManager;
import endetect.com.endetect.utity.UtilManager;
import in.ddinfotech.shiksha.Util.PrefencesManager;

public class DashBoardFragment extends Fragment {
    View view;
    DrawerLayout drawer_layout;
    RelativeLayout rlDrawer;
    SectionListAdapter adapter;
    // MyClickListner myClickListner;
    boolean isSectionPosition = true;
    int sectionPosition = 0, newSectionPosition = 0;
    DashBoardAdapter dashBoardAdapter;
    EndUserAdapter adapter2;
    Typeface typeface;
    ImageView barTextView, barTextViewRightSide;
    ArrayList<SectionListObject> sectionList = new ArrayList<>();
    ArrayList<DashBoardObject> mainList = new ArrayList<>();
    ArrayList<DashBoardObject> resultsFilter = new ArrayList<>();
    //ArrayList<QuestionSetObject> newQuestionList = new ArrayList<>();
    RecyclerView recyclerViewDrawer, recycler_main, recyclerViewDrawerRight;
    ApiCallBack apiCallBack;
    PrefencesManager prefencesManager;
    UtilManager utilManager;
    CoordinatorLayout activity_candidate_Dashboard;
    SessionManager sessionManager;
    RecyclerView.LayoutManager manager;
    boolean loading;
    int pageNum = 0;
    String feedNo = "1", ENDUSERID;
    SearchView svAlertDropDown;
    private final static String TAG_FRAGMENT = "TAG_FRAGMENT";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = LayoutInflater.from(getActivity()).inflate(R.layout.activity_main2, null);
        initClass();
        initRecyclerViewMain(view);
        initResponse();


        LoadData(String.valueOf(pageNum),"");


        svAlertDropDown = view.findViewById(R.id.svAlertDropDown);
        svAlertDropDown.setIconifiedByDefault(false);
        svAlertDropDown.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filter(newText);
                return false;
            }
        });

        return view;
    }

    private void initRecyclerViewMain(View view) {
        recycler_main = view.findViewById(R.id.rv_main);
        manager = new LinearLayoutManager(getActivity());
        recycler_main.setLayoutManager(manager);
        recycler_main.setHasFixedSize(true);
        dashBoardAdapter = new DashBoardAdapter(getActivity(), mainList);
        recycler_main.setAdapter(dashBoardAdapter);
        recycler_main.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    int visibleItemCount = manager.getChildCount();
                    int totalItemCount = manager.getItemCount();
                    int pastVisibleItems = ((LinearLayoutManager) recycler_main.getLayoutManager()).findFirstVisibleItemPosition();
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                        if (!loading) {
                            loading = true;
                            pageNum++;
                            LoadData(String.valueOf(pageNum),"");
                        } else {
                            loading = false;
                        }
                    }

                }

            }
        });
    }

    private void initClass() {
        sessionManager = new SessionManager(getActivity());
        prefencesManager = new PrefencesManager(getActivity());
        utilManager = new UtilManager(getActivity());
    }

    private void LoadData(String pageNo,String endUserId) {
//        MainActivity activity = (MainActivity)getActivity();
//        String id = activity.initView();
        Bundle bundle = getArguments();
        if (bundle != null) {
            ENDUSERID = bundle.getString(AppConstant.PreferenceKey.ENDUSERID);
        } else if (endUserId.equals("")){
            feedNo = prefencesManager.getString(AppConstant.PreferenceKey.FEEDNO);
            ENDUSERID = "";
        }else {
            mainList.clear();
//            feedNo = prefencesManager.getString(AppConstant.PreferenceKey.FEEDNO);
            ENDUSERID = endUserId;
        }

        new AllAsyncTaskRequest(getActivity(), apiCallBack).setListener(AppConstant.ApiCall.GET_FEEDS, 1).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, prefencesManager.getString(AppConstant.PreferenceKey.QNWER_ID), feedNo, "1", pageNo, ENDUSERID);
    }

    @Override
    public void onResume() {
        super.onResume();
        itemSelect();
    }

    private void itemSelect() {
        (dashBoardAdapter).setOnItemClickListner(new MyClickListner() {
            @Override
            public void onItemClick(int position, View view) {
                switch (view.getId()) {
                    case R.id.iv_iv_activity_sc:
                    DashBoardObject object = mainList.get(position);
                    if (object.getDecdata() != null) {
                        Toast.makeText(getActivity(), "Image Clicked", Toast.LENGTH_SHORT).show();
                        utilManager.zoomImageWithUrl(object.getDecdata());
                    }
                    break;
                    case R.id.iv_drop:
//                        registerForContextMenu(view);
                        ImageView imageView = (recycler_main).findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.iv_drop);
                        PopupMenu popup = new PopupMenu(getActivity(), imageView);		// replace imageView with your view where user click
                        popup.getMenu().add(Menu.NONE, 1 ,Menu.NONE, "Add to watchlist");
                        popup.getMenu().add(Menu.NONE, 2 ,Menu.NONE, "Add Note");
                        popup.getMenu().add(Menu.NONE, 3 ,Menu.NONE, "Edit User");
                        popup.getMenu().add(Menu.NONE, 4 ,Menu.NONE, "Delete");

                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                Toast.makeText(getActivity(), "clicked "+item.getTitle(), Toast.LENGTH_SHORT).show();
                                int position=item.getItemId();

////                        Intent intent=new Intent(context, ActivityName.class);
////                        intent.putExtra("pageId",String.valueOf(position));
////                        intent.putExtra("value",item.getTitle());
//                                if (intent!=null){
//                                    context.startActivity(intent);
//                                }
                                return true;
                            }
                        });
                        popup.show();
                        break;
                    case R.id.tv_title:
                        String id =mainList.get(position).getEnduserid();
                        LoadData("",String.valueOf(id));
//                        Bundle bundle = new Bundle();
//                        bundle.putString("pageNo", prefencesManager.getString(AppConstant.PreferenceKey.pageNum));
//                        bundle.putString(AppConstant.PreferenceKey.FEEDNO, prefencesManager.getString(AppConstant.PreferenceKey.FEEDNO));
//                        bundle.putString("ENDUSERID",id);
//                        DashBoardFragment dashBoardFragment = new DashBoardFragment();
//                        dashBoardFragment.setArguments(bundle);
//                        final FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
//                        transaction.replace(R.id.frame_layout, dashBoardFragment, TAG_FRAGMENT);
//                        transaction.addToBackStack(null);
//                        transaction.commit();
                    break;
                }

            }
        });
    }


    private void initResponse() {
        this.apiCallBack = new ApiCallBack() {
            @Override
            public void callBackResponse(String result, int requestCode) {
                switch (requestCode) {
                    case 1:
//                        dissmissProgress();
                        Log.wtf("result dashboard", result);
                        if (result != null && !result.equals("")) {
                            try {
                                Log.wtf("result dashboard", result);
                                JSONArray jsonArray = new JSONArray(result);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    String status = jsonObject.getString("status");
                                    if (status.equalsIgnoreCase("1")) {
                                        DashBoardObject o = new DashBoardObject(
                                                jsonObject.getString("feedsno"),
                                                jsonObject.getString("enduserid"),
                                                jsonObject.getString("endownerid"),
                                                jsonObject.getString("dtype"),
                                                jsonObject.getString("app_title"),
                                                jsonObject.getString("other"),
                                                jsonObject.getString("mouse_clicks"),
                                                jsonObject.getString("wordcount"),
                                                jsonObject.getString("linecount"),
                                                jsonObject.getString("decdata"),
                                                jsonObject.getString("delete"),
                                                jsonObject.getString("ttype")
                                        );
                                        if (i == jsonArray.length() - 1) {
                                            String feedsno = jsonObject.getString("feedsno");
                                            prefencesManager.saveString(AppConstant.PreferenceKey.FEEDNO, feedsno);
                                            prefencesManager.saveString(AppConstant.PreferenceKey.pageNum, String.valueOf(pageNum));
                                        }
                                        mainList.add(o);
                                        dashBoardAdapter.notifyDataSetChanged();
                                    } else {
                                        Toast.makeText(getActivity(), "Pleaase check User Name and Passwod", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                Toast.makeText(getActivity(), "some error...", Toast.LENGTH_SHORT).show();
                            }
                        }
                        break;

                }
            }

            @Override
            public void callBackError(final Exception exception, final int requestCode) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        //dissmissProgress();
//                        //throwExceptions(exception, activity_candidate_Dashboard);
//
//                    }
//                });
                System.out.println("result" + exception);
            }
        };
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        mainList.clear();
        if (charText.length() == 0) {
            mainList.addAll(resultsFilter);
            //hideSoftKeyboard();
        } else {
            for (DashBoardObject wp : resultsFilter) {
                if (wp.getDtype().toLowerCase(Locale.getDefault()).contains(charText) ||
                        wp.getLast_seen().toLowerCase(Locale.getDefault()).contains(charText) ||
                        wp.getActivity().toLowerCase(Locale.getDefault()).contains(charText)) {
                    mainList.add(wp);
                }
            }
        }
        dashBoardAdapter.notifyDataSetChanged();
    }


//    @Override
//    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
//        super.onCreateContextMenu(menu, v, menuInfo);
//        menu.setHeaderTitle("");
//        menu.add(0, v.getId(), 0, "Add to watchlist");
//        menu.add(0, v.getId(), 0, "Add Note");
//        menu.add(0, v.getId(), 0, "Edit User");
//        menu.add(0, v.getId(), 0, "Delete");
//    }
//    @Override
//    public boolean onContextItemSelected(MenuItem item) {
//        Toast.makeText(getActivity(), "Selected Item: " +item.getTitle(), Toast.LENGTH_SHORT).show();
//        return true;
//    }
}
